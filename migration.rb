#!/usr/bin/ruby

require 'json'
require 'shellwords'
begin
    require 'io/console'
    $have_console = true
rescue Exception
    $have_console = false
end

class Semaphore
    def initialize(count)
        @count = count
        @lock = Mutex.new
        @waiting = []
    end

    def take()
        while true
            @lock.synchronize do
                if @count > 0
                    @count -= 1
                    return
                end
                condvar = Thread::ConditionVariable::new
                @waiting << condvar
                condvar.wait(@lock)
            end
        end
    end

    def release()
        @lock.synchronize do
            @count += 1
            if condvar = @waiting.shift
                condvar.signal
            end
        end
    end
end

class Array
    def include_all?(other_array)
        other_array.difference(self).empty?
    end

    def include_any?(other_array)
        !self.intersection(other_array).empty?
    end
end

$guest_image = nil
$guest_login = 'arch:arch'
$guest_uid_gid = '1000:1000'
$qemu_command = nil
$virtiofsd_command = 'virtiofsd'
$scratch_dir = '/tmp'
$dry_run = false
$jobs = 1
$run_only = []
$with = []
$without = []
$repeat = 1
$random = nil
$random_order = false

arg_context = nil
ARGV.each do |arg|
    if arg[..1] == '--' && arg_context
        $stderr.puts("No argument provided for #{arg_context}")
        exit 1
    end
    if arg =~ /^--[a-z-]+=/
        arg_context, arg = arg.split('=', 2)
    end

    case arg
    when '--guest-image', '--guest-login', '--guest-uid-gid', '--jobs', '--qemu-command', '--random', '--repeat',
            '--scratch-dir', '--virtiofsd-command', '--with', '--without'
        arg_context = arg
    when '--dry-run'
        $dry_run = true
    when '--help'
        puts <<EOF
Usage: #{__FILE__} [options...] [to run...]

Takes optional options, and test indices to select specific tests to run.

Specifying the --guest-image option is mandatory.

Options:
  --dry-run: Do not actually run any test, but list everything that would be run

  --guest-image <path>: Path to a guest image [mandatory]
        This image will not be modified, but only used as the basis for a scratch overlay that is deleted after testing.

  --guest-login <user:pass>: Guest SSH login in the user:password form [default: #{$guest_login}]

  --guest-uid-gid <uid:gid>: Guest UID and GID for the given login; note that if either UID or GID differs from the
        host, having virtiofsd create any file from the guest will fail unless:
          - we pass a UID:GID map, but this only works when running virtiofsd not as root and in a namespace sandbox
          - virtiofsd is run as root (i.e. for tests using file handles), but then it will create all files as the guest
            UID:GID, so we refuse to do this if the UID does not match
        All tests not matching these criteria are cancelled.
        [default: #{$guest_uid_gid}]

  --help: Print this message and exit

  --jobs <n>: Set number of parallel jobs [default: #{$jobs}]

  --qemu-command <cmd>: Command to invoke QEMU [default: auto-guess]

  --random <n>: Run a random subset of n tests instead of all tests [default: disabled]

  --random-order: Run tests in random order [default: no]

  --repeat <n>: Repeat the set of tests n times [default: #{$repeat}]

  --scratch-dir <dir>: Scratch directory to use for testing [default: #{$scratch_dir}]

  --virtiofsd-command <cmd>: Command to invoke virtiofsd [default: #{$virtiofsd_command}]

  --with <case>[,<case2>[,...]]: Run only tests including all of the given cases; can be specified multiple times,
        in which case all tests including any of the separate combinations are run
        [default: run all]

  --without <case>[,<case2>[,...]]: Don’t run tests including all of the given cases; can be specified multiple times,
        in which case none of the tests including any of the separate combinations are run
        [default: run all]
EOF
        exit 0
    when '--random-order'
        $random_order = true
    else
        case arg_context
        when '--guest-image'
            $guest_image = arg
        when '--guest-login'
            $guest_login = arg
        when '--guest-uid-gid'
            $guest_uid_gid = arg
        when '--jobs'
            $jobs = Integer(arg)
        when '--qemu-command'
            $qemu_command = arg
        when '--random'
            $random = Integer(arg)
        when '--repeat'
            $repeat = Integer(arg)
            if $repeat <= 0
                $stderr.puts("--repeat argument must be positive")
                exit 1
            end
        when '--scratch-dir'
            $scratch_dir = arg
        when '--virtiofsd-command'
            $virtiofsd_command = arg
        when '--with'
            $with << arg.split(',').map { |a| a.strip.to_sym }
        when '--without'
            $without << arg.split(',').map { |a| a.strip.to_sym }
        when nil
            $run_only << Integer(arg)
        else
            $stderr.puts("Internal error: Unknown arg context #{arg_context}")
            exit 1
        end
        arg_context = nil
    end
end

if arg_context
    $stderr.puts("No argument given for option #{arg_context}")
    exit 1
end

if !$guest_image && !$dry_run
    $stderr.puts("No guest image specified")
    exit 1
end

$run_only = nil if $run_only.empty?
$vm_boot_snapshot = "#{$scratch_dir}/post-boot.vmsnapshot"
$vm_boot_snapshot_complete = "#{$scratch_dir}/post-boot.vmsnapshot.complete"
guest_user, guest_pass = $guest_login.split(':', 2)
$guest_uid, $guest_gid = $guest_uid_gid.split(':', 2)
$host_uid = `id -u`.strip
$host_gid = `id -g`.strip

BASE_CONFIG = {
    'shared-directory': "#{$scratch_dir}/shared-dir",
    'guest-image': $guest_image,
    'guest-user': guest_user,
    'guest-password': guest_pass,
    'virtiofsd-binary': $virtiofsd_command,
    'qemu-binary': $qemu_command,
    'source-arguments': [],
    'destination-arguments': [],
    'vm-boot-snapshot': $vm_boot_snapshot,
    'vm-boot-snapshot-complete': $vm_boot_snapshot_complete,
    'guest-mountpoint': '/tmp/mnt',
    'pre-migrate-operations': [{ name: 'Random virtio-fs ops', cmd: 'find /tmp/mnt > /dev/null' }],
    'during-migrate-operations': [],
    'pre-switchover-operations': [],
    'post-migrate-operations': [{ cmd: 'touch /tmp/migration-done' }],
    'post-failed-migrate-operations': [{ cmd: 'touch /tmp/migration-done' }],
}

OPTIONAL_CONFIGS = {
    test_random_data: {
        'pre-migrate-operations': [
            { cmd: 'dd if=/dev/urandom of=/tmp/template bs=4k count=1024 status=none' },
        ],

        'during-migrate-operations': [
            { name: 'Filling test file',
              cmd: 'iterations=0
                    while [ ! -f /tmp/migration-done ]; do
                        dd if=/tmp/template of=/tmp/mnt/test-file bs=4k conv=notrunc oflag=append,direct status=none
                        iterations=$((iterations + 1))
                    done
                    echo $iterations > /tmp/iterations',
              background: true },
        ],

        'post-migrate-operations': [
            { name: 'Comparing hashes',
              cmd: 'while [ ! -f /tmp/iterations ]; do sleep 0.1; done
                    iterations=$(cat /tmp/iterations)
                    ref_hash=$((for i in $(seq $iterations); do cat /tmp/template; done) | md5sum | sed -e "s/\s.*//")
                    is_hash=$(md5sum /tmp/mnt/test-file | sed -e "s/\s.*//")
                    echo $is_hash > /tmp/mnt/guest-hash
                    if [ "$ref_hash" != "$is_hash" ]; then
                        echo "ERROR: Hashes differ!"
                        echo "Expected: $ref_hash"
                        echo "Is:       $is_hash"
                        echo "($iterations copy iterations)"
                    else
                        echo "OK: Hashes are equal."
                    fi' },
            { name: 'Comparing hash on host',
              cmd: "ref_hash=$(cat guest-hash)
                    is_hash=$(md5sum test-file | sed -e 's/\\s.*//')
                    if [ \"$ref_hash\" != \"$is_hash\" ]; then
                        echo 'ERROR: Hashes differ!'
                        echo \"Guest: $ref_hash\"
                        echo \"Host:  $is_hash\"
                    else
                        echo 'OK: Hashes are equal.'
                    fi",
              on: 'host' },
        ],
    },

    test_unlink: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-unlinked file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/unlink-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/unlink-file"
                    touch /tmp/unlink-written',
              background: true },
            { name: 'Reading from to-be-unlinked file',
              cmd: 'while [ ! -f /tmp/mnt/unlink-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/unlink-partially-written ]; do sleep 0.1; done
                        echo \"-- [unlink] partial file --\"
                        cat /tmp/mnt/unlink-file
                        touch /tmp/mnt/unlink-pre-read
                        while [ ! -f /tmp/unlink-written ]; do sleep 0.1; done
                        echo \"-- [unlink] full file (opened pre-unlink) --\"
                        cat
                    ) < /tmp/mnt/unlink-file" &> /tmp/unlink-read-output
                    touch /tmp/unlink-post-read',
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Unlinking file',
              cmd: "while [ ! -f unlink-pre-read ]; do sleep 0.1; done
                    rm unlink-file",
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/unlink-post-read ]; do sleep 0.1; done
                    cat /tmp/unlink-read-output
                    echo "-- [unlink] full file (opened post-migration) --"
                    cat /tmp/mnt/unlink-file 2>&1' }
        ],
    },

    test_temporary: {
        'during-migrate-operations': [
            { name: 'Writing to temporary file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/temporary-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/temporary-file"
                    touch /tmp/temporary-written',
              background: true },
            { name: 'Reading from and unlinking temporary file',
              cmd: 'while [ ! -f /tmp/mnt/temporary-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/temporary-partially-written ]; do sleep 0.1; done
                        echo \"-- [temporary] partial file --\"
                        cat /tmp/mnt/temporary-file
                        rm /tmp/mnt/temporary-file
                        while [ ! -f /tmp/temporary-written ]; do sleep 0.1; done
                        echo \"-- [temporary] full file (opened post-unlink) --\"
                        cat
                    ) < /tmp/mnt/temporary-file" &> /tmp/temporary-read-output
                    touch /tmp/temporary-post-read',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/temporary-post-read ]; do sleep 0.1; done
                    cat /tmp/temporary-read-output
                    echo "-- [temporary] inode should be fully gone post-migration --"
                    cat /tmp/mnt/temporary-file 2>&1' },
        ],
    },

    test_rename: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-renamed file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/rename-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/rename-file"
                    touch /tmp/rename-written',
              background: true },
            { name: 'Reading from to-be-renamed file',
              cmd: 'while [ ! -f /tmp/mnt/rename-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/rename-partially-written ]; do sleep 0.1; done
                        echo \"-- [rename] partial file --\"
                        cat /tmp/mnt/rename-file
                        touch /tmp/mnt/rename-pre-read
                        while [ ! -f /tmp/rename-written ]; do sleep 0.1; done
                        echo \"-- [rename] full file (opened pre-rename) --\"
                        cat
                    ) < /tmp/mnt/rename-file" &> /tmp/rename-read-output
                    touch /tmp/rename-post-read',
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Renaming file',
              cmd: "while [ ! -f rename-pre-read ]; do sleep 0.1; done
                    mv rename-file{,.renamed}",
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/rename-post-read ]; do sleep 0.1; done
                    cat /tmp/rename-read-output
                    echo "-- [rename] full file (opened post-migration) --"
                    cat /tmp/mnt/rename-file 2>&1
                    echo "-- [rename] full renamed file (opened post-migration) --"
                    cat /tmp/mnt/rename-file.renamed' },
        ],
    },

    test_in_guest_rename: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-renamed file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/in-guest-rename-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/in-guest-rename-file"
                    touch /tmp/in-guest-rename-written',
              background: true },
            { name: 'Reading from to-be-renamed file',
              cmd: 'while [ ! -f /tmp/mnt/in-guest-rename-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/in-guest-rename-partially-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-rename] partial file --\"
                        cat /tmp/mnt/in-guest-rename-file
                        mv /tmp/mnt/in-guest-rename-file{,.renamed}
                        while [ ! -f /tmp/in-guest-rename-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-rename] full file (opened pre-rename) --\"
                        cat
                    ) < /tmp/mnt/in-guest-rename-file" &> /tmp/in-guest-rename-read-output
                    touch /tmp/in-guest-rename-post-read',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/in-guest-rename-post-read ]; do sleep 0.1; done
                    cat /tmp/in-guest-rename-read-output
                    echo "-- [in-guest-rename] full file (opened post-migration) --"
                    cat /tmp/mnt/in-guest-rename-file 2>&1
                    echo "-- [in-guest-rename] full renamed file (opened post-migration) --"
                    cat /tmp/mnt/in-guest-rename-file.renamed' },
        ],
    },

    # Same as rename, but move to subdirectory
    test_move: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-moved file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/move-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/move-file"
                    touch /tmp/move-written',
              background: true },
            { name: 'Reading from to-be-moved file',
              cmd: 'while [ ! -f /tmp/mnt/move-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/move-partially-written ]; do sleep 0.1; done
                        echo \"-- [move] partial file --\"
                        cat /tmp/mnt/move-file
                        touch /tmp/mnt/move-pre-read
                        while [ ! -f /tmp/move-written ]; do sleep 0.1; done
                        echo \"-- [move] full file (opened pre-move) --\"
                        cat
                    ) < /tmp/mnt/move-file" &> /tmp/move-read-output
                    touch /tmp/move-post-read',
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Moving file',
              cmd: "mkdir -p movedir
                    while [ ! -f move-pre-read ]; do sleep 0.1; done
                    mv {,movedir/}move-file",
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/move-post-read ]; do sleep 0.1; done
                    cat /tmp/move-read-output
                    echo "-- [move] full file (opened post-migration) --"
                    cat /tmp/mnt/move-file 2>&1
                    echo "-- [move] full moved file (opened post-migration) --"
                    cat /tmp/mnt/movedir/move-file' },
        ],
    },

    # Same as rename, but move to subdirectory
    test_in_guest_move: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-moved file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/in-guest-move-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/in-guest-move-file"
                    touch /tmp/in-guest-move-written',
              background: true },
            { name: 'Reading from to-be-moved file',
              cmd: 'while [ ! -f /tmp/mnt/in-guest-move-file ]; do sleep 0.1; done
                    sh -c "(
                        mkdir -p /tmp/mnt/movedir
                        while [ ! -f /tmp/in-guest-move-partially-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-move] partial file --\"
                        cat /tmp/mnt/in-guest-move-file
                        mv /tmp/mnt/{,movedir/}in-guest-move-file
                        while [ ! -f /tmp/in-guest-move-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-move] full file (opened pre-move) --\"
                        cat
                    ) < /tmp/mnt/in-guest-move-file" &> /tmp/in-guest-move-read-output
                    touch /tmp/in-guest-move-post-read',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/in-guest-move-post-read ]; do sleep 0.1; done
                    cat /tmp/in-guest-move-read-output
                    echo "-- [in-guest-move] full file (opened post-migration) --"
                    cat /tmp/mnt/in-guest-move-file 2>&1
                    echo "-- [in-guest-move] full moved file (opened post-migration) --"
                    cat /tmp/mnt/movedir/in-guest-move-file' },
        ],
    },

    test_move_outside_shared_dir: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-moved file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/move-outside-shared-dir-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/move-outside-shared-dir-file"
                    touch /tmp/move-outside-shared-dir-written',
              background: true },
            { name: 'Reading from to-be-moved file',
              cmd: 'while [ ! -f /tmp/mnt/move-outside-shared-dir-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/move-outside-shared-dir-partially-written ]; do sleep 0.1; done
                        echo \"-- [move-outside-shared-dir] partial file --\"
                        cat /tmp/mnt/move-outside-shared-dir-file
                        touch /tmp/mnt/move-outside-shared-dir-pre-read
                        while [ ! -f /tmp/move-outside-shared-dir-written ]; do sleep 0.1; done
                        echo \"-- [move-outside-shared-dir] full file (opened pre-move) --\"
                        cat
                    ) < /tmp/mnt/move-outside-shared-dir-file" &> /tmp/move-outside-shared-dir-read-output
                    touch /tmp/move-outside-shared-dir-post-read',
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Moving file',
              cmd: "while [ ! -f move-outside-shared-dir-pre-read ]; do sleep 0.1; done
                    mv {,../}move-outside-shared-dir-file",
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/move-outside-shared-dir-post-read ]; do sleep 0.1; done
                    cat /tmp/move-outside-shared-dir-read-output
                    echo "-- [move-outside-shared-dir] full file (opened post-migration) --"
                    cat /tmp/mnt/move-outside-shared-dir-file 2>&1' },
        ],
    },

    test_replace: {
        'during-migrate-operations': [
            { name: 'Creating replacer file',
              cmd: "echo BAD > replace-file.source",
              on: 'host' },
            { name: 'Writing to replacee file',
              cmd: 'sh -c "(echo foo
                        touch /tmp/replace-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/replace-file"
                    touch /tmp/replace-written',
              background: true },
            { name: 'Reading from replacee file',
              cmd: 'while [ ! -f /tmp/mnt/replace-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/replace-partially-written ]; do sleep 0.1; done
                        echo \"-- [replace] partial file --\"
                        cat /tmp/mnt/replace-file
                        touch /tmp/mnt/replace-pre-read
                        while [ ! -f /tmp/replace-written ]; do sleep 0.1; done
                        echo \"-- [replace] full file (opened pre-replace) --\"
                        cat
                    ) < /tmp/mnt/replace-file" &> /tmp/replace-read-output
                    touch /tmp/replace-post-read',
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Replacing file',
              cmd: "while [ ! -f replace-pre-read ]; do sleep 0.1; done
                    mv replace-file{.source,}",
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/replace-post-read ]; do sleep 0.1; done
                    cat /tmp/replace-read-output
                    echo "-- [replace] full file (opened post-migration) --"
                    cat /tmp/mnt/replace-file' },
        ],
    },

    test_in_guest_replace: {
        'during-migrate-operations': [
            { name: 'Creating replacer file',
              cmd: "echo BAD > /tmp/mnt/in-guest-replace-file.source" },
            { name: 'Writing to replacee file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/in-guest-replace-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/in-guest-replace-file"
                    touch /tmp/in-guest-replace-written',
              background: true },
            { name: 'Reading from replacee file',
              cmd: 'while [ ! -f /tmp/mnt/in-guest-replace-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/in-guest-replace-partially-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-replace] partial file --\"
                        cat /tmp/mnt/in-guest-replace-file
                        mv /tmp/mnt/in-guest-replace-file{.source,}
                        while [ ! -f /tmp/in-guest-replace-written ]; do sleep 0.1; done
                        echo \"-- [in-guest-replace] full file (opened pre-replace) --\"
                        cat
                    ) < /tmp/mnt/in-guest-replace-file" &> /tmp/in-guest-replace-read-output
                    touch /tmp/in-guest-replace-post-read',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/in-guest-replace-post-read ]; do sleep 0.1; done
                    cat /tmp/in-guest-replace-read-output
                    echo "-- [in-guest-replace] full file (opened post-migration) --"
                    cat /tmp/mnt/in-guest-replace-file' },
        ],
    },

    test_permission_loss: {
        'during-migrate-operations': [
            { name: 'Writing to to-be-inaccessible file',
              cmd: 'sh -c "(
                        echo foo
                        touch /tmp/permission-partially-written
                        while [ ! -f /tmp/migration-done ]; do sleep 0.1; done
                        echo bar
                    ) > /tmp/mnt/permission-file"
                    touch /tmp/permission-written',
              background: true },
            { name: 'Reading from to-be-inaccessible file',
              cmd: 'while [ ! -f /tmp/mnt/permission-file ]; do sleep 0.1; done
                    sh -c "(
                        while [ ! -f /tmp/permission-partially-written ]; do sleep 0.1; done
                        echo \"-- [permission] partial file --\"
                        cat /tmp/mnt/permission-file
                        touch /tmp/mnt/permission-pre-read
                        while [ ! -f /tmp/permission-written ]; do sleep 0.1; done
                        echo \"-- [permission] full file (opened while accessible) --\"
                        cat
                    ) < /tmp/mnt/permission-file" &> /tmp/permission-read-output
                    touch /tmp/permission-post-read',
              background: true },
            # Try to have the file match the host UID/GID.  This will most likely fail if virtiofsd is not run as root,
            # but in that case we ensure that all files in the shared directory are created with the host user UID/GID
            # anyway (namespace case: --[ug]id-map; no sandbox: run everything inside the guest as root, preventing
            # virtiofsd from UID/GID switching) the guest UID/GID).
            # If virtiofsd is run as root, we have neither of those options, and just accept that everything is created
            # with the guest UID/GID, though we cancel those tests if the guest UID does not match the host UID.  This
            # very test here also cares about the GID, though: When the destination virtiofsd instance runs in a
            # sandbox, it apparently has CAP_DAC_OVERRIDE, but only for files whose UID and GID match that of virtiofsd.
            # Therefore, if host and guest GID match, the destination can open the file regardless of permission bits.
            # If they do not match, it cannot.  While we'd rather prefer to test the latter case, we already have it
            # covered if the destination simply does not run in a sandbox, so we just need to get a consistent result
            # here, which we get if we have the GID match that of the host.
            { name: 'Ensuring file UID/GID match host',
              cmd: "while [ ! -f /tmp/mnt/permission-file ]; do sleep 0.1; done;
                    sudo -n chown #{$host_uid}:#{$host_gid} /tmp/mnt/permission-file 2>/dev/null",
              background: true },
        ],
        'pre-switchover-operations': [
            { name: 'Making file inaccessible',
              cmd: 'while [ ! -f permission-pre-read ]; do sleep 0.1; done
                    chmod 0000 permission-file',
              on: 'host',
              background: true },
        ],
        'post-migrate-operations': [
            { name: 'Producing read output',
              cmd: 'while [ ! -f /tmp/permission-post-read ]; do sleep 0.1; done
                    cat /tmp/permission-read-output
                    echo "-- [permission] full file (opened post-migration) --"
                    cat /tmp/mnt/permission-file 2>&1' },
        ],
    },

    # Continue migration to a third VM, setting on-restore-error=abort on it
    test_migrate_onwards_strict: {
        instances: 3,
        'dest-2-arguments': ['--migration-on-error=abort'],
        'pre-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'during-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'post-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'post-failed-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
    },

    # Continue migration to a third VM, setting on-restore-error=guest-error on it
    test_migrate_onwards_permissive: {
        instances: 3,
        'dest-2-arguments': ['--migration-on-error=guest-error'],
        'pre-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'during-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'post-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
        'post-failed-migrate-2-operations': [{ cmd: 'ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /"' }],
    },

    cache_always: {
        # Note that caching is actually an argument for the guest, effectively, so we only need to set it on the source
        # (where the filesystem is originally mounted)
        'source-arguments': ['--cache=always'],
    },

    confirm_paths: {
        'source-arguments': ['--migration-confirm-paths'],
    },

    fail_to_guest: {
        'destination-arguments': ['--migration-on-error=guest-error'],
    },

    no_sandbox: {
        'source-arguments': ['--sandbox=none'],
        'destination-arguments': ['--sandbox=none'],
    },

    mixed_sandbox: {
        'destination-arguments': ['--sandbox=none'],
    },

    verify_handles: {
        'source-arguments': ['--migration-verify-handles'],
    },

    file_handles: {
        'source-arguments': ['--inode-file-handles=mandatory'],
        'destination-arguments': ['--inode-file-handles=mandatory'],
    },

    mixed_file_handles: {
        'source-arguments': ['--inode-file-handles=mandatory'],
    },

    mixed_read_only: {
        'destination-arguments': ['--readonly'],
    },

    migration_mode_file_handles: {
        'source-arguments': ['--migration-mode=file-handles'],
        'destination-arguments': ['--migration-mode=file-handles', '--modcaps=+dac_read_search'],
        'dest-2-arguments': ['--modcaps=+dac_read_search'],
    },

    migration_hard_links: {
        'source-arguments': ['--migration-hard-links=SANDBOX_SHARED_DIR'],
        'destination-arguments': ['--migration-hard-links=SANDBOX_SHARED_DIR'],
        'dest-2-arguments': ['--migration-hard-links=SANDBOX_SHARED_DIR'],
    },
}

def do_run(job)
    def merge_dict(into, from)
        raise 'Wrong type' unless into.kind_of?(Hash) && from.kind_of?(Hash)
        from.each do |key, value|
            if into[key]
                if value.kind_of?(Hash)
                    into[key] = into[key].dup
                    merge_dict(into[key], value)
                elsif value.kind_of?(Array)
                    raise 'Wrong type' unless into[key].kind_of?(Array)
                    into[key] = into[key].dup + value.dup
                else
                    into[key] = value.dup
                end
            else
                into[key] = value.dup
            end
        end
    end

    config = BASE_CONFIG.dup
    job[:combination].each do |c|
        merge_dict(config, OPTIONAL_CONFIGS[c])
    end
    if !job[:combination].include?(:cache_always)
        # Make testing more predictable than 'auto' would allow
        merge_dict(config, { 'destination-arguments': ['--cache=never'] })
    end
    config[:'post-failed-migrate-operations'] = config[:'post-migrate-operations'].dup
    if $guest_uid != $host_uid || $guest_gid != $host_gid
        # virtiofsd tries to switch to the guest UID/GID for creating files.
        # When not run as root, that will just fail (if UID/GID differs from its own), so we need to find a way to
        # inhibit switching or map the guest UID/GID to that of virtiofsd.
        # When run as root, it will work, but we don't want to create files under a UID that is not that of the current
        # user, so if that were to be the case, we cancel the job.

        def ensure_uid_for(config, side)
            arguments_key = case side
                            when :source
                                :'source-arguments'
                            when :destination
                                :'destination-arguments'
                            when :dest_2
                                :'dest-2-arguments'
                            end

            if config[arguments_key]
                merge_dict(config, { arguments_key => [
                    '--translate-uid', "squash-guest:0:#{$host_uid}:4294967295",
                    '--translate-gid', "squash-guest:0:#{$host_gid}:4294967295",
                    '--translate-uid', "host:#{$host_uid}:#{$guest_uid}:1",
                    '--translate-gid', "host:#{$host_gid}:#{$guest_gid}:1",
                ]})
            end
        end

        ensure_uid_for(config, :source)
        ensure_uid_for(config, :destination)
        ensure_uid_for(config, :dest_2)
    end

    config[:'shared-directory'] = job[:shared_dir]
    config[:'source-virtiofsd-socket'] = "#{job[:job_dir]}/vfsdsock-src"
    config[:'destination-virtiofsd-socket'] = "#{job[:job_dir]}/vfsdsock-dst"
    config[:'dest-2-virtiofsd-socket'] = "#{job[:job_dir]}/vfsdsock-dst-2"
    config[:'job-index'] = job[:job_index]
    config[:'migration-url'] = "unix:#{job[:job_dir]}/migration.sock"

    [:'source-arguments', :'destination-arguments', :'dest-2-arguments'].each do |arg_key|
        if config[arg_key]
            sandbox_shared_dir = config[arg_key].include?('--sandbox=none') ?  job[:shared_dir] : '/'
            config[arg_key].map! { |arg| arg.sub('SANDBOX_SHARED_DIR', sandbox_shared_dir) }
        end
    end

    system("mkdir -p #{job[:shared_dir].shellescape}")
    output_file = "#{job[:job_dir]}/output"
    progress_pipe = File.pipe
    pid = fork
    if !pid
        output = File.open(output_file, 'w')
        progress_pipe[0].close
        $stdin.reopen('/dev/null', 'r')
        $stdout.reopen(output)
        $stderr.reopen(output)
        output.close
        progress_pipe[1].close_on_exec = false
        config['progress-fd'] = progress_pipe[1].fileno

        json_filename = "#{job[:job_dir]}/config.json"
        IO.write(json_filename, JSON.unparse(config))

        ENV['LANG'] = 'C'
        exec("#{File.realpath(File.dirname(__FILE__))}/helpers/do-migration.rb", json_filename)
    end
    job[:child_pid] = pid
    progress_pipe[1].close
    while true
        p = ''
        begin
            c = nil
            while true
                c = progress_pipe[0].getc
                break if !c || c == "\n"
                p += c
            end
            break if !c
        rescue Exception => e
            break
        end
        $global_job_lock.synchronize do
            job[:state] = p
            update_output()
        end
    end
    progress_pipe[0].close
    Process.waitpid(pid)

    result = IO.read(output_file) \
        .gsub("\r", '') \
        .gsub(/\d{4}(-\d\d){2}T(\d\d:)\d\d\S* /, '') \
        .gsub(/[^\n]*\[INFO\s+virtiofsd\][^\n]*\n/, '') \
        .gsub(/[^\n]*Couldn't set the process uid as root[^\n]*\n/, '') \
        .gsub(/[^\n]*Couldn't set the process gid as root[^\n]*\n/, '') \
        .gsub(/[^\n]*failed to change uid back to root[^\n]*\n/, '') \
        .gsub(/[^\n]*failed to change gid back to root[^\n]*\n/, '') \
        .gsub(/[^\n]*failed to add 'DAC_OVERRIDE'[^\n]*\n/, '') \
        .gsub(/[^\n]*Failure when trying to set the limit to[^\n]*\n/, '') \
        .gsub(/virtio-user-fs device \S*\/virtio-backend/, 'virtio-user-fs device .../virtio-backend') \
        .gsub(/instance 0x\h* of device '[\h:.]*\/vhost-user-fs/, "instance X of device 'X/vhost-user-fs'") \
        .gsub(/[^\n]*proc_paths[^\n]*Failed to get path from \/proc\/self\/fd: Inode is invalid[^\n]*\n/, '') \
        .gsub(/[^\n]*Connection to \S* closed by remote host\.[^\n]*\n/, '') \
        .gsub(/([Ii]node|[Hh]andle) \d+/, '\1 X') \
        .gsub(/st_dev=\d+, mnt_id=\d+, st_ino=\d+/, 'st_dev=X, mnt_id=Y, st_ino=Z') \
        .gsub(/File handle differs: (\h\h )+!=( \h\h)+/, 'File handle differs: X != Y') \
        .gsub(/Inode ID differs: Expected \d+, found \d+/, 'Inode ID differs: Expected X, found Y') \
        .gsub(/\s*\(os error [^)]*\)/, '') \
        .gsub(/\.virtiofsd\.inode\d+/, '.virtiofsd.inodeX')

    system("rm -rf #{job[:job_dir].shellescape}")

    result
end

# Note that any occurrence of SRC_VIOFSD_ROOT_DIR and DST_VIOFSD_ROOT_DIR will be replaced by the respective virtiofsd
# root directory (which depends on sandboxing) before running the test.
combinations_to_run = {
    # Simple sanity test
    [] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
END_OF_OUTPUT
    ),

    # Continue filling up a file through migration, check expected content afterwards (via MD5 hash)
    [:test_random_data] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, before migration] dd if=/dev/urandom of=/tmp/template bs=4k count=1024 status=none ---
--- [source, during migration] Filling test file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Comparing hashes ---
OK: Hashes are equal.
--- [host, after migration] Comparing hash on host ---
OK: Hashes are equal.
END_OF_OUTPUT
    ),

    # Unlink file (during migration) that is open in the guest, migrate via paths without any special switches.
    # Migration is expected to fail because the destination cannot find the file by path.
    [:test_unlink] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Opening DST_VIOFSD_ROOT_DIR/unlink-file: No such file or directory
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
foo
bar
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
foo
bar
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Create temporary file, i.e. write to it and unlink while in use, migrate via paths without any special switches.
    # Migration is expected to fail because the destination cannot find the file by path.
    [:test_temporary] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
[WARN  virtiofsd::passthrough] Unlinked [shared directory root]/temporary-file, failed to get new path: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/temporary-file); will be unable to migrate inode
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
foo
bar
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
foo
bar
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Rename file (during migration) that is open in the guest, migrate via paths without any special switches.
    # Migration is expected to fail because the destination cannot find the file by path.
    [:test_rename] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Opening DST_VIOFSD_ROOT_DIR/rename-file: No such file or directory
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
foo
bar
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
foo
bar
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # Rename file (during migration) in the guest, migrate via paths without any special switches.  The rename must
    # always be picked up by virtiofsd and be noted in the migration stream so the destination can open the renamed
    # file.
    [:test_in_guest_rename] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [in-guest-rename] partial file --
foo
-- [in-guest-rename] full file (opened pre-rename) --
foo
bar
-- [in-guest-rename] full file (opened post-migration) --
cat: /tmp/mnt/in-guest-rename-file: No such file or directory
-- [in-guest-rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
    ),

    # Move file to a sub-directory (during migration) that is open in the guest, migrate via paths without any special
    # switches.  Migration is expected to fail because the destination cannot find the file by path.
    [:test_move] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Opening DST_VIOFSD_ROOT_DIR/move-file: No such file or directory
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
foo
bar
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
foo
bar
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # Move file to a sub-directory (during migration) in the guest, migrate via paths without any special switches.  The
    # move must always be picked up by virtiofsd and be noted in the migration stream so the destination can open the
    # moved file.
    [:test_in_guest_move] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [in-guest-move] partial file --
foo
-- [in-guest-move] full file (opened pre-move) --
foo
bar
-- [in-guest-move] full file (opened post-migration) --
cat: /tmp/mnt/in-guest-move-file: No such file or directory
-- [in-guest-move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
    ),

    # Effectively the same as :test_move, but the file is moved outside of the shared directory.  Here, it doesn't
    # make a difference, only with :confirm_paths.
    # TODO: With multi-writer live migration features in place, the guest probably will not lose access to the moved
    # file, though it could be argued that the guest should see a copy of it instead of retaining access to the
    # original.
    [:test_move_outside_shared_dir] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Opening DST_VIOFSD_ROOT_DIR/move-outside-shared-dir-file: No such file or directory
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
foo
bar
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
foo
bar
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Clear permission bits during migration, migrate via paths without any special switches.  Without CAP_DAC_OVERRIDE,
    # migration is expected to fail.  Because permission bits are stored in the inode, we expect this to remain this way
    # even with additional features for multi-writer live migration.
    [:test_permission_loss] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-inaccessible file ---
--- [source, during migration] Reading from to-be-inaccessible file ---
--- [source, during migration] Ensuring file UID/GID match host ---
--- [host, pre-switchover] Making file inaccessible ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Opening inode X (DST_VIOFSD_ROOT_DIR/permission-file) as handle X: Permission denied
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [permission] partial file --
foo
-- [permission] full file (opened while accessible) --
foo
bar
-- [permission] full file (opened post-migration) --
cat: /tmp/mnt/permission-file: Permission denied
END_OF_OUTPUT
    ),

    # Same as :test_unlink, but when the destination recognizes the error, migration will continue instead of aborting
    # and any further access to the file will result in a guest error.
    [:fail_to_guest, :test_unlink] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Opening DST_VIOFSD_ROOT_DIR/unlink-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/unlink-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/unlink-file: No such file or directory
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
foo
bar
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Same as :test_temporary, but when the destination recognizes the error, migration will continue instead of
    # aborting and any further access to the file will result in a guest error.
    [:fail_to_guest, :test_temporary] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
[WARN  virtiofsd::passthrough] Unlinked [shared directory root]/temporary-file, failed to get new path: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/temporary-file); will be unable to migrate inode
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
foo
bar
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Same as :test_rename, but when the destination recognizes the error, migration will continue instead of aborting
    # and any further access to the file will result in a guest error.
    # (Note that the file only contains "foo" here, even though it should contain "foo\nbar" -- that is because the
    # post-migration write fails.)
    [:fail_to_guest, :test_rename] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Opening DST_VIOFSD_ROOT_DIR/rename-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/rename-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/rename-file: No such file or directory
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
foo
bar
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # Same as :test_move, but when the destination recognizes the error, migration will continue instead of aborting
    # and any further access to the file will result in a guest error.
    # (Note that the file only contains "foo" here, even though it should contain "foo\nbar" -- that is because the
    # post-migration write fails.)
    [:fail_to_guest, :test_move] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Opening DST_VIOFSD_ROOT_DIR/move-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/move-file: No such file or directory
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Opening DST_VIOFSD_ROOT_DIR/move-file: No such file or directory
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
foo
bar
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # Same as :test_permission_loss, but when the destination recognizes the error, migration will continue instead of
    # aborting and any further access to the file will result in a guest error.
    [:fail_to_guest, :test_permission_loss] => (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-inaccessible file ---
--- [source, during migration] Reading from to-be-inaccessible file ---
--- [source, during migration] Ensuring file UID/GID match host ---
--- [host, pre-switchover] Making file inaccessible ---
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X (DST_VIOFSD_ROOT_DIR/permission-file) as handle X: Permission denied
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X (DST_VIOFSD_ROOT_DIR/permission-file) as handle X: Permission denied
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [permission] partial file --
foo
-- [permission] full file (opened while accessible) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [permission] full file (opened post-migration) --
cat: /tmp/mnt/permission-file: Permission denied
END_OF_OUTPUT
    ),

    # Replace a file that is open in the guest by some other file, migrate via paths without any special switches.
    # Migration is expected to succeed, but open the wrong file, so reading from the file is expected to produce a bad
    # result (mix of the replacer and replacee file).
    # Note that this (data corruption) is not what we want, but it is what is bound to happen in this configuration.
    [:test_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
BAD
bar
-- [replace] full file (opened post-migration) --
BAD
bar
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    [:test_in_guest_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
[WARN  virtiofsd::passthrough] Overwrote (via rename) [shared directory root]/in-guest-replace-file, failed to get new path: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/in-guest-replace-file); will be unable to migrate inode
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [in-guest-replace] partial file --
foo
-- [in-guest-replace] full file (opened pre-replace) --
foo
bar
-- [in-guest-replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [in-guest-replace] partial file --
foo
-- [in-guest-replace] full file (opened pre-replace) --
foo
bar
-- [in-guest-replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    [:test_in_guest_replace, :fail_to_guest] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
[WARN  virtiofsd::passthrough] Overwrote (via rename) [shared directory root]/in-guest-replace-file, failed to get new path: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/in-guest-replace-file); will be unable to migrate inode
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [in-guest-replace] partial file --
foo
-- [in-guest-replace] full file (opened pre-replace) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [in-guest-replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [in-guest-replace] partial file --
foo
-- [in-guest-replace] full file (opened pre-replace) --
foo
bar
-- [in-guest-replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    # Replace a file that is open in the guest by some other file, migrate with --migration-verify-handles.  The
    # destination is expected to catch that the file has been replaced and report an error, preventing data corruption.
    # However, re-opening the file after migration will produce the output of the replacer file.
    # TODO: For ideal multi-writer live migration: Same as :test_replace
    [:verify_handles, :test_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Inode X is not the same inode as in the migration source: File handle differs: X != Y
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    # Same as :verify_handles + :test_replace, but here, the source will note the mismatch.
    [:confirm_paths, :test_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/replace-file): File handle differs: X != Y; looking it up through /proc/self/fd
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/replace-file)
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    # Same as :verify_handles + :test_replace, but when the destination recognizes the error, migration will continue
    # instead of aborting and any further access to the file will result in a guest error.  Opening the file anew will
    # produce the unmodified output of the replacer file ("BAD").
    [:verify_handles, :fail_to_guest, :test_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Inode X is not the same inode as in the migration source: File handle differs: X != Y
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Inode X is not the same inode as in the migration source: File handle differs: X != Y
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Inode X is not the same inode as in the migration source: File handle differs: X != Y
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    # Same as :verify_handles + :fail_to_guest + :test_replace, but here, the source will note the mismatch.
    [:confirm_paths, :fail_to_guest, :test_replace] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/replace-file): File handle differs: X != Y; looking it up through /proc/self/fd
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/replace-file)
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [host, during migration] Creating replacer file ---
--- [source, during migration] Writing to replacee file ---
--- [source, during migration] Reading from replacee file ---
--- [host, pre-switchover] Replacing file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [replace] partial file --
foo
-- [replace] full file (opened pre-replace) --
foo
bar
-- [replace] full file (opened post-migration) --
BAD
END_OF_OUTPUT
        ),
    },

    # :confirm_paths does not allow following an unlinked file.
    [:confirm_paths, :test_unlink] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/unlink-file): No such file or directory; looking it up through /proc/self/fd
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/unlink-file)
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
foo
bar
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-unlinked file ---
--- [source, during migration] Reading from to-be-unlinked file ---
--- [host, pre-switchover] Unlinking file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [unlink] partial file --
foo
-- [unlink] full file (opened pre-unlink) --
foo
bar
-- [unlink] full file (opened post-migration) --
cat: /tmp/mnt/unlink-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # :confirm_paths does not allow following an unlinked file.
    [:confirm_paths, :test_temporary] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
[WARN  virtiofsd::passthrough] Unlinked [shared directory root]/temporary-file, failed to get new path: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/temporary-file); will be unable to migrate inode
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/temporary-file)
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
foo
bar
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to temporary file ---
--- [source, during migration] Reading from and unlinking temporary file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [temporary] partial file --
foo
-- [temporary] full file (opened post-unlink) --
foo
bar
-- [temporary] inode should be fully gone post-migration --
cat: /tmp/mnt/temporary-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # :confirm_paths allows us to (potentially) find a file that's been renamed by an external party.  It works here.
    [:confirm_paths, :test_rename] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/rename-file): No such file or directory; looking it up through /proc/self/fd
[INFO  virtiofsd::passthrough::device_state::preserialization::proc_paths] Found inode X: [shared directory root]/rename-file.renamed
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
foo
bar
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-renamed file ---
--- [source, during migration] Reading from to-be-renamed file ---
--- [host, pre-switchover] Renaming file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [rename] partial file --
foo
-- [rename] full file (opened pre-rename) --
foo
bar
-- [rename] full file (opened post-migration) --
cat: /tmp/mnt/rename-file: No such file or directory
-- [rename] full renamed file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # Like with :test_rename, :confirm_paths should successfully find the file here.
    [:confirm_paths, :test_move] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/move-file): No such file or directory; looking it up through /proc/self/fd
[INFO  virtiofsd::passthrough::device_state::preserialization::proc_paths] Found inode X: [shared directory root]/movedir/move-file
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
foo
bar
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move] partial file --
foo
-- [move] full file (opened pre-move) --
foo
bar
-- [move] full file (opened post-migration) --
cat: /tmp/mnt/move-file: No such file or directory
-- [move] full moved file (opened post-migration) --
foo
bar
END_OF_OUTPUT
        ),
    },

    # In this case, :confirm_paths does not work, because the file is now outside of the shared directory.
    [:confirm_paths, :test_move_outside_shared_dir] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/move-outside-shared-dir-file): No such file or directory; looking it up through /proc/self/fd
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Got empty path for non-root node, so it is outside the shared directory
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination)), is source side (source) still running?)
--- [source, after failed migration] touch /tmp/migration-done ---
--- [source, after failed migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
foo
bar
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
foo
bar
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
    },

    # Same as :confirm_paths + :test_move_outside_shared_dir, but turning the on-migration error into guest errors
    # instead of aborting migration.  While the error does occur on the source side here (:confirm_paths occurs on the
    # source), it still should be transmitted to the destination for the latter to decide what to do based on
    # --on-restore-error.
    [:confirm_paths, :fail_to_guest, :test_move_outside_shared_dir] => {
        single_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
[WARN  virtiofsd::passthrough::device_state::preserialization::proc_paths] Lost inode X (former location: [shared directory root]/move-outside-shared-dir-file): No such file or directory; looking it up through /proc/self/fd
[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Got empty path for non-root node, so it is outside the shared directory
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid handle X is open in guest: Opening inode X as handle X: Inode is invalid because of an error during the preceding migration, which was: Migration source has lost inode X
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
cat: -: Input/output error
cat: closing standard input: Input/output error
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
        multi_writer: (<<END_OF_OUTPUT
--- [source, before migration] Random virtio-fs ops ---
--- [source, during migration] Writing to to-be-moved file ---
--- [source, during migration] Reading from to-be-moved file ---
--- [host, pre-switchover] Moving file ---
(Migration successful)
--- [destination, after migration] touch /tmp/migration-done ---
--- [destination, after migration] Producing read output ---
-- [move-outside-shared-dir] partial file --
foo
-- [move-outside-shared-dir] full file (opened pre-move) --
foo
bar
-- [move-outside-shared-dir] full file (opened post-migration) --
cat: /tmp/mnt/move-outside-shared-dir-file: No such file or directory
END_OF_OUTPUT
        ),
    },
}

new_combinations = {}
combinations_to_run.each do |combination, expected_results|
    # Any options that double-check the inode during switch-over don't make much sense with migration-mode=file-handles
    if !combination.include_any?([:confirm_paths, :verify_handles])
        new_combinations[combination + [:migration_mode_file_handles]] = expected_results.dup
    end
end
combinations_to_run.merge!(new_combinations)

combinations_to_run.each do |combination, expected_results|
    if expected_results.kind_of?(Hash)
        if combination.include?(:migration_mode_file_handles)
            combinations_to_run[combination] = expected_results[:multi_writer]
        elsif combination.include?(:migration_hard_links)
            combinations_to_run[combination] = expected_results[:multi_writer]
        else
            combinations_to_run[combination] = expected_results[:single_writer]
        end
    end
end

# In-guest renames must always be picked up regardless of which switches we use, so the output should always be the same
combinations_to_run[[:verify_handles, :confirm_paths, :test_in_guest_rename]] =
    combinations_to_run[[:test_in_guest_rename]]

# Create some :cache_always variations:
#  - :test_replace: Reading the replaced file after a failed migration should return the original content, not the
#                   replaced version of the file.  (This is assuming that the migration failed because the replacement
#                   was detected and aborted.)
#                   The same applies on successful migration with proper multi-writer measures, UNLESS the destination
#                   uses file handles, because then the file handle will go stale when the original file is deleted
#                   (:file_handles is added only later, though, so the reference output update is done there).
#                   Without those measures, we will get the replaced version, UNLESS the migration was successful due to
#                   :fail_to_guest, in which case there will be an EBADF.
#  - :test_unlink, :test_temporary, :test_rename, :test_move:
#                  After a successful --on-restore-error=abort migration where the rename was followed (resulting in
#                  WARN messages), reading the original file path should succeed because it is still cached.
#                  In contrast, with --on-restore-error=guest-error, reading it will yield EBADF instead of ENOENT,
#                  UNLESS this is :test_temporary, because then the guest knows the file is gone.
new_combinations = {}
combinations_to_run.each do |combination, expected_result|
    if combination.include?(:test_replace)
        if expected_result.include?('(Migration failed') ||
                combination.include_any?([:migration_mode_file_handles, :migration_hard_links])
            new_combinations[combination + [:cache_always]] = expected_result \
                .sub("-- [replace] full file (opened post-migration) --\nBAD",
                     "-- [replace] full file (opened post-migration) --\nfoo\nbar")
        elsif combination.include?(:fail_to_guest)
            new_combinations[combination + [:cache_always]] = expected_result \
                .sub("-- [replace] full file (opened post-migration) --\nBAD",
                     "-- [replace] full file (opened post-migration) --\ncat: /tmp/mnt/replace-file: Input/output error")
        else
            new_combinations[combination + [:cache_always]] = expected_result.dup
        end
    elsif combination.include_any?([:test_unlink, :test_temporary, :test_rename, :test_move]) &&
            expected_result.include?('(Migration successful)') &&
            expected_result.include?('WARN  virtiofsd::passthrough::device_state::serialization')
        if combination.include?(:fail_to_guest)
            if combination.include?(:test_temporary)
                expected = "cat: \\1: No such file or directory"
            else
                expected = "cat: \\1: Input/output error"
            end
        else
            expected = "foo\nbar"
        end
        new_combinations[combination + [:cache_always]] = expected_result \
            .sub(/cat: (.*): No such file or directory/, expected)
    end
end
combinations_to_run.merge!(new_combinations)

new_combinations = {}
combinations_to_run.each do |combination, expected_result|
    new_combinations[combination + [:no_sandbox]] = expected_result.dup
    new_combinations[combination + [:mixed_sandbox]] = expected_result.dup
end
combinations_to_run.merge!(new_combinations)

new_combinations = {}
combinations_to_run.each do |combination, expected_result|
    # A) We can only test --readonly on the destination, not the source.  Every single test has to write some data to
    #    the shared directory on the source, so no test will work with --readonly there.  Hence we only have
    #    :mixed_read_only, not pure :read_only.
    # B) :test_random_data must be able to continue writing data to the shared directory throughout and after migration.
    #    Therefore, it doesn't work with --readonly.
    if !combination.include?(:test_random_data)
        new_combinations[combination + [:mixed_read_only]] = expected_result.dup
    end
end
combinations_to_run.merge!(new_combinations)

new_combinations = {}
combinations_to_run.each do |combination, expected_result|
    # Try to migrate onwards (when migration was successful, doesn't make sense otherwise)

    if expected_result.include?('(Migration successful')
        # Successful on-migration, no problems
        success_append = <<EOF
--- [destination, before migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
--- [destination, during migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
(Migration successful)
--- [destination #2, after migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
EOF

        # Encountered an issue during on-migration: See that we still continue migration, just signal to the destination
        # that the inode is invalid, and the destination can decide what to do.  The destination is in strict mode
        # (on-restore-error=abort), so will abort migration.
        strict_error_append = <<EOF
--- [destination, before migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
--- [destination, during migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[ERROR virtiofsd::vhost_user] Migration failed: Failed to load state: Inode X: Migration source has lost inode X
qemu-system-x86_64: Error loading back-end state of virtio-user-fs device .../virtio-backend (tag: "testfs"): Back-end failed to process its internal state
qemu-system-x86_64: Failed to load vhost-user-fs-backend:back-end
qemu-system-x86_64: error while loading state for instance X of device 'X/vhost-user-fs''
qemu-system-x86_64: load of migration failed: Input/output error
(Migration failed (Migration failed on destination side (destination #2)), is source side (destination) still running?)
--- [destination, after failed migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
EOF

        # Encountered an issue during on-migration: See that we still continue migration, just signal to the destination
        # that the inode is invalid, and the destination can decide what to do.  The destination is in permissive mode
        # (on-restore-error=guest-error), so will take the inode, and continue to list it as invalid.
        permissive_error_append = <<EOF
--- [destination, before migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
--- [destination, during migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
[WARN  virtiofsd::passthrough::device_state::serialization] Failed to serialize inode X (st_dev=X, mnt_id=Y, st_ino=Z): Failed to reconstruct inode location; marking as invalid
[WARN  virtiofsd::passthrough::device_state::deserialization] Invalid inode X indexed: Migration source has lost inode X
(Migration successful)
--- [destination #2, after migration] ls -l /tmp/mnt/overlay.qcow2 | sed -e "s/ .* / /" ---
-rw-r--r-- /tmp/mnt/overlay.qcow2
EOF

        # If the original destination VM failed to open some node, and, not aborting migration, noted it down as
        # invalid, on an onwards migration we expect it to again be the cause of some trouble (with --cache=always,
        # because otherwise, the guest will have already forgotten it).
        if expected_result.include?('Invalid inode X indexed') && combination.include?(:cache_always) && !combination.include?(:test_temporary)
            strict_result = expected_result + strict_error_append
            permissive_result = expected_result + permissive_error_append
        else
            strict_result = expected_result + success_append
            permissive_result = strict_result.dup
        end
        new_combinations[combination + [:test_migrate_onwards_strict]] = strict_result
        new_combinations[combination + [:test_migrate_onwards_permissive]] = permissive_result
    end
end
combinations_to_run.merge!(new_combinations)

# Inject file handle variants immediately after their non-file variants so they get run regularly and a potential
# `sudo -n` timer will not run out
order_to_run = combinations_to_run.keys
order_to_run.each do |combination|
    expected_result = combinations_to_run[combination]
    combinations_to_run[combination + [:file_handles]] = expected_result.dup
    # See :cache_always block: With file handles, the file handle of the replaced file will go stale, so the destination
    # will always see the replaced file, regardless of caching mode
    # (Won’t happen with hard-links-based migration: The hard link will still exist, so no ESTALE.)
    if combination.include_all?([:test_replace, :cache_always, :migration_mode_file_handles])
        combinations_to_run[combination + [:file_handles]] \
                .sub!("-- [replace] full file (opened post-migration) --\nfoo\nbar",
                      "-- [replace] full file (opened post-migration) --\nBAD")
    end
    combinations_to_run[combination + [:mixed_file_handles]] = expected_result.dup
end
order_to_run = order_to_run.map { |c| [c, c + [:file_handles], c + [:mixed_file_handles]] }.flatten(1)

# Fix up reference outputs for specific combinations where necessary
combinations_to_run.each do |combination, expected_result|
    # When we test moving an inode outside of the shared directory together with :confirm_paths, we get different errors
    # with and without a sandbox around the source instance.
    # (:mixed_sandbox only puts the destination in a sandbox.)
    if combination.include_all?([:test_move_outside_shared_dir, :confirm_paths, :no_sandbox])
        expected_result \
            .sub!('Failed to get path from /proc/self/fd: Got empty path for non-root node, so it is outside the ' \
                  'shared directory',
                  "Path \"JOB_ROOT_DIR/move-outside-shared-dir-file\" is outside the directory " \
                  "(\"DST_VIOFSD_ROOT_DIR\")")
    end

    # --cache=always can have slightly different behavior when using file handles: While file handles are (just like
    # O_PATH FDs) "resistant" to renames, if their respective inode has been removed, they will become stale (whereas
    # having an FD open forces the inode to remain even with nlink == 0).  In the replace test, the original file is
    # removed, so when using file handles, the guest trying to open it after migration will receive an ESTALE.  This
    # error is actually handled within the guest kernel and not passed to guest applications: It will refresh its
    # (cached) entry, i.e. do a new lookup, which now finds the replaced file.  Therefore, when using file handles,
    # when reading the file after migration, we see the replaced file's content, even with --cache=always.
    # (Note this is about the source side, which is where :mixed_file_handles uses file handles, too.)
    if combination.include_all?([:test_replace, :cache_always]) &&
            combination.include_any?([:file_handles, :mixed_file_handles]) &&
            expected_result.include?('(Migration failed')
        expected_result \
            .sub!("-- [replace] full file (opened post-migration) --\nfoo\nbar",
                  "-- [replace] full file (opened post-migration) --\nBAD")
    end

    # Removing access rights will not prevent the destination from opening a file if it has CAP_DAC_OVERRIDE.  This is
    # true when run as root (:file_handles, :migration_mode_file_handles), or when run in a namespace sandbox (neither
    # :no_sandbox nor :mixed_sandbox).  In these cases, migration will succeed
    if combination.include?(:test_permission_loss) &&
            (combination.include_any?([:file_handles, :migration_mode_file_handles]) ||
             !combination.include_any?([:no_sandbox, :mixed_sandbox]))
        expected_result = expected_result \
            .gsub(/\[ERROR virtiofsd::vhost_user\][^\n]*Opening inode X[^\n]*: Permission denied\n/, '') \
            .gsub(/\[WARN  [^\]]*deserialization\][^\n]*Opening inode X[^\n]*: Permission denied\n/, '') \
            .gsub(/qemu-system-x86_64:[^\n]*\n/, '') \
            .gsub(/\(Migration failed[^\n]*/, '(Migration successful)') \
            .gsub('source, after failed migration', 'destination, after migration') \
            .gsub("-- [permission] full file (opened while accessible) --\n" \
                  "cat: -: Input/output error\ncat: closing standard input: Input/output error",
                  "-- [permission] full file (opened while accessible) --\nfoo\nbar")
    end

    if combination.include_all?([:migration_hard_links, :confirm_paths, :test_temporary]) &&
            combination.include_any?([:file_handles, :mixed_file_handles])
        # --migration-hard-links ensures all open handles have a path.  This can change --migration-confirm-path’s
        # behavior for test_temporary, because it can then find a path when it otherwise could not.
        # For some reason, it only finds that path when using file handles, not sure why.
        expected_result.gsub!('[ERROR virtiofsd::passthrough::device_state::preserialization::proc_paths] Inode X: Failed to get path from /proc/self/fd: Inode deleted (SRC_VIOFSD_ROOT_DIR/temporary-file)',
                              '[INFO  virtiofsd::passthrough::device_state::preserialization::proc_paths] Found inode X: [shared directory root]/.virtiofsd.inodeX')
    end

    # When (successfully) migrating to a read-only instance, the destination will not be able to write the final line
    # ("bar") into the test file.
    if combination.include?(:mixed_read_only) && expected_result.include?('(Migration successful)')
        expected_result.gsub!("\nbar", '')
    end

    combinations_to_run[combination] = expected_result
end

$total = order_to_run.length
stats = {
    done: 0,
    succeeded: 0,
    failed: 0,
    skipped: 0,
    cancelled: 0,
}

$total_digits = Math.log10($total).to_i + 1

$active_jobs = []
$done_job_queue = []
$output_done_job_queue = []
$last_active_job_table_height = nil
$last_active_job_count = nil
$global_job_lock = Mutex.new

# Must be called with $global_job_lock held
def update_output()
    term_h = term_w = nil
    if $have_console
        begin
            term_h, term_w = STDOUT.winsize
        rescue Exception
        end
    end

    if $last_active_job_table_height && $last_active_job_table_height > 0
        # Use \r\e[A instead if \e[F; the former is more widely supported
        $stdout.write("\r\e[#{$last_active_job_table_height}A")
    end
    $stdout.write("\e[J")
    while job = $output_done_job_queue.shift
        comb_str = job[:combination].empty? ? '(default)' : job[:combination] * ' + '
        result_str = case job[:result]
                     when :succeeded
                         " \e[1;32mOK\e[0m "
                     when :failed
                         "\e[1;31mFAIL\e[0m"
                     when :skipped
                         "\e[1mSKIP\e[0m"
                     when :cancelled
                         "\e[1;33mCNCL\e[0m"
                     when :interrupted
                         "\e[1mINTR\e[0m"
                     end

        puts("#{job[:state]} #{result_str} [#{'%*i' % [$total_digits, job[:i] + 1]}/#{$total}] #{comb_str}")
        case job[:result]
        when :failed
            f_pref = "#{$scratch_dir}/#{job[:i]}-#{job[:job_index]}-"
            IO.write("#{f_pref}-expected", job[:expected])
            IO.write("#{f_pref}-actual", job[:output])
            diff=`diff --color=always #{"#{f_pref}-expected".shellescape} #{"#{f_pref}-actual".shellescape}`.strip
            File.delete("#{f_pref}-expected")
            File.delete("#{f_pref}-actual")
            puts <<EOF
=== \e[1mExpected output\e[0m ===
#{job[:expected]}
=== \e[1mActual output\e[0m ===
#{job[:output]}
=== \e[1mdiff\e[0m ===
#{diff}
EOF
        when :cancelled
            puts("     -- #{job[:reason]}") if job[:reason]
        when :interrupted
            puts("=== \e[1mInterrupted output\e[0m ===")
            puts(job[:output])
        end
    end

    $last_active_job_table_height = 0
    $active_jobs.each do |job|
        comb_str = job[:combination].empty? ? '(default)' : job[:combination] * ' + '
        puts("#{job[:state]}      [#{'%*i' % [$total_digits, job[:i] + 1]}/#{$total}] #{comb_str}")

        if term_w
            # State (2) + Result (6) + Index (3 + 2 * digits) + space (1)
            # (Cannot use job_str.length: Contains ANSI sequences)
            job_line_len = 12 + 2 * $total_digits + comb_str.length
            job[:display_height] = (job_line_len + term_w - 1) / term_w
        else
            job[:display_height] = 1
        end
        $last_active_job_table_height += job[:display_height]
    end
end

def run_test_job(job)
    if $dry_run
        job[:result] = :skipped
        return
    end

    job[:output] = do_run(job)
    if job[:output].kind_of?(Hash)
        job.merge!(job[:output])
    elsif job[:output] =~ /Interrupt( \(RuntimeError\)|)$/
        job[:result] = :interrupted
    elsif job[:output].strip == job[:expected].strip
        job[:result] = :succeeded
    elsif job[:output].include?('sudo: a password is required')
        job[:result] = :cancelled
        job[:reason] = 'Test requires running virtiofsd as root, but sudo requires a password'
    else
        job[:result] = :failed
    end
end

threads = []
job_indices = $jobs.times.to_a
$thread_sem = Semaphore.new($jobs)

$got_intr_once = false
$interrupted = false
Signal.trap('INT') do
    $interrupted = true
    $active_jobs.each do |job|
        Process.kill('INT', job[:child_pid]) if job[:child_pid]
    end
    exit 0 if $got_intr_once
    $got_intr_once = true
end

$have_vm_post_boot_snapshot = false
if $dry_run
    $have_vm_post_boot_snapshot = true
end

all_tests = order_to_run.length.times.to_a
run_queue = []
if $run_only
    run_queue += ($run_only.map { |i| i - 1 } & all_tests).sort
end
if $random
    run_queue += all_tests.sample($random).sort
end
if run_queue.empty?
    run_queue = all_tests
end
if !$with.empty?
    run_queue.select! { |i|
        $with.any? { |combination| order_to_run[i].include_all?(combination) }
    }
end
if !$without.empty?
    run_queue.reject! { |i|
        $without.any? { |combination| order_to_run[i].include_all?(combination) }
    }
end
run_queue *= $repeat

if $random_order
    run_queue.shuffle!
end

run_queue.each do |i|
    combination = order_to_run[i]
    expected_result = combinations_to_run[combination]

    $thread_sem.take
    threads.reject! { |t| t.join(0) }
    if job_indices.empty?
        $stderr.puts("Internal error: Taking the thread semaphore should have released a thread")
        exit 1
    end
    $global_job_lock.synchronize do
        while job = $done_job_queue.shift
            if job[:result] == :interrupted
                next
            end
            stats[job[:result]] += 1
            stats[:done] += 1
        end
    end
    break if $interrupted
    job_index = job_indices.shift

    job = {
        state: '  ',
        result: nil,
        i: i,
        job_index: job_index,
        combination: combination.dup,
        expected: expected_result.dup,
        job_dir: "#{$scratch_dir}/#{job_index}",
        shared_dir: "#{$scratch_dir}/#{job_index}/shared-dir",
    }

    threads << Thread.new {
        if combination.include?(:no_sandbox)
            job[:expected].gsub!('SRC_VIOFSD_ROOT_DIR', job[:shared_dir])
            job[:expected].gsub!('DST_VIOFSD_ROOT_DIR', job[:shared_dir])
        elsif combination.include?(:mixed_sandbox)
            job[:expected].gsub!('SRC_VIOFSD_ROOT_DIR', '/')
            job[:expected].gsub!('DST_VIOFSD_ROOT_DIR', job[:shared_dir])
        else
            job[:expected].gsub!('SRC_VIOFSD_ROOT_DIR', '/')
            job[:expected].gsub!('DST_VIOFSD_ROOT_DIR', '/')
        end
        job[:expected].gsub!('JOB_ROOT_DIR', "#{$scratch_dir}/#{job_index}")
        job[:expected].gsub!('//', '/')

        $global_job_lock.synchronize do
            $active_jobs << job
            update_output()
        end

        if job[:job_index] > 0 && !$have_vm_post_boot_snapshot
            while !$have_vm_post_boot_snapshot && !$interrupted
                $have_vm_post_boot_snapshot = File.file?($vm_boot_snapshot_complete)
                sleep(1)
            end
        end

        if !$interrupted
            run_test_job(job)
        else
            job[:result] = :interrupted
        end
        $global_job_lock.synchronize do
            $active_jobs -= [job]
            $done_job_queue << job
            $output_done_job_queue << job
            update_output()
        end

        job_indices << job[:job_index]
        $thread_sem.release
    }
end

while !threads.empty?
    $thread_sem.take
    threads.reject! { |t| t.join(0) }
    while job = $done_job_queue.shift
        next if job[:result] == :interrupted
        stats[job[:result]] += 1
        stats[:done] += 1
    end
end

system("rm -f #{$vm_boot_snapshot.shellescape} #{$vm_boot_snapshot_complete.shellescape}")

puts
puts("Ran #{stats[:done]} test#{stats[:done] != 1 ? 's' : ''}, #{stats[:succeeded]} successful, #{stats[:failed]} failed, #{stats[:cancelled]} cancelled, #{stats[:skipped]} skipped")
