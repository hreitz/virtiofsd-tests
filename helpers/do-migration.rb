#!/usr/bin/ruby

require (File.realpath(File.dirname(__FILE__)) + '/qemu.rb')
require 'json'
require 'shellwords'

if ARGV[0][0] == '{'
    $test_config = JSON.parse(ARGV[0])
else
    $test_config = JSON.parse(IO.read(ARGV[0]))
end
$shared_dir = $test_config['shared-directory']
if !$shared_dir
    $stderr.puts("Expecting shared-directory key to point to a test directory")
    exit 1
end

$guest_img = $test_config['guest-image']
if !$guest_img
    $stderr.puts("Expecting guest-image key to point to a base guest image")
    exit 1
end
$guest_img_format = $test_config['guest-image-format']
if !$guest_img_format
    $guest_img_format = File.extname($guest_img).sub(/^\./, '')
    if $guest_img_format == 'img'
        $guest_img_format = 'raw'
    end
end

$guest_user = $test_config['guest-user']
$guest_password = $test_config['guest-password']
if !$guest_user || !$guest_password
    $stderr.puts("Expecting guest-user and guest-password keys")
    exit 1
end

instance_count = $test_config['instances'] || 2
if instance_count < 2
    $stderr.puts("instance-count must be at least 2")
end
$instances = [
    {
        name: 'source',
        key: 'source',
    },
    {
        name: 'destination',
        key: 'destination',
    },
]
(instance_count - 2).times do |i|
    $instances << {
        name: "destination ##{i + 2}",
        key: "dest-#{i + 2}",
    }
end

$job_index = $test_config['job-index'] || 0
$virtiofsd_binary = $test_config['virtiofsd-binary'] || 'virtiofsd'
$qemu_binary = $test_config['qemu-binary'] || "qemu-system-#{`uname -m`}".strip
$qemu_machine_type = $test_config['qemu-machine-type'] || `#{$qemu_binary.shellescape} --machine help | grep '(default)' | head -n 1 | sed -e 's/ \+.*//'`.strip
$instances.each.with_index do |inst, i|
    inst[:sock] = $test_config["#{inst[:key]}-virtiofsd-socket"] || "/tmp/vfsdsock#{i}"
    inst[:args] = $test_config["#{inst[:key]}-arguments"] || []
end
$vfs_tag = $test_config['virtiofs-tag'] || 'testfs'
$guest_mountpoint = $test_config['guest-mountpoint'] || '/tmp/mnt'

$migrations = []
(instance_count - 1).times do |i|
    key = i == 0 ? '' : "-#{i + 1}"
    $migrations[i] = {
        pre: $test_config["pre-migrate#{key}-operations"] || [{ 'cmd' => "ls #{$guest_mountpoint.shellescape}" }],
        during: $test_config["during-migrate#{key}-operations"] || [{ 'cmd' => "ls #{$guest_mountpoint.shellescape}" }],
        pre_switchover: $test_config["pre-switchover#{key}-operations"] || [],
        post: $test_config["post-migrate#{key}-operations"] || [{ 'cmd' => "ls #{$guest_mountpoint.shellescape}" }],
        post_failed: $test_config["post-failed-migrate#{key}-operations"] || [{ 'cmd' => "ls #{$guest_mountpoint.shellescape}" }],
    }
end
$migration_url = $test_config['migration-url'] || "tcp:127.0.0.1:#{50413 + $job_index}"
$vm_boot_snapshot = $test_config['vm-boot-snapshot']
$vm_boot_snapshot_complete = $test_config['vm-boot-snapshot-complete']

if $test_config['progress-fd']
    $progress_fd = IO.new($test_config['progress-fd'])
    $progress_fd.puts('  ')
    $progress_fd.flush
else
    $progress_fd = nil
end

if !system("qemu-img create -f qcow2 #{$shared_dir.shellescape}/overlay.qcow2 -b #{$guest_img.shellescape} -F #{$guest_img_format.shellescape} >/dev/null")
    $stderr.puts("Failed to create #{inst[:name]} guest overlay")
    exit 1
end

$instances.each do |inst|
    begin
        File.unlink(inst[:sock])
    rescue Exception
    end
    begin
        File.unlink("#{inst[:sock]}.pid")
    rescue Exception
    end
end

with_sudo = $instances.any? { |inst| inst[:args].include?('--inode-file-handles=mandatory') || inst[:args].include?('--migration-mode=file-handles') }
if with_sudo && !system('sudo -n true')
    exit 1
end

$instances.each.with_index do |inst, i|
    inst[:viofsd] = fork
    if !inst[:viofsd]
        use_fh = inst[:args].include?('--inode-file-handles=mandatory')
        mig_fh = i > 0 && $instances[i - 1][:args].include?('--migration-mode=file-handles')
        super_command = (use_fh || mig_fh) ? ['sudo', '-n'] : []
        exec(*super_command,
             $virtiofsd_binary,
             "--socket-path=#{inst[:sock]}",
             "--shared-dir=#{$shared_dir}",
             '--announce-submounts',
             '--log-level=info',
             *inst[:args])
        exit 1
    end
end

max_time = 100 # 10 s
while !$instances.all? { |inst| File.socket?(inst[:sock]) } && max_time > 0
    sleep(0.1)
    max_time -= 1
end
timeout_insts = $instances.select { |inst| !File.socket?(inst[:sock]) }.map { |inst| inst[:name] }
if !timeout_insts.empty?
    $stderr.puts("Timeout launching #{timeout_insts * ', '} virtiofsd")
    exit 1
end

if with_sudo
    $instances.each do |inst|
        system("sudo -n chown #{`id -u`.strip}:#{`id -g`.strip} #{inst[:sock].shellescape}{,.pid}")
        system("sudo -n chmod ugo+rw #{inst[:sock].shellescape}{,.pid}")
    end
end

if $progress_fd
    $progress_fd.puts('🌑')
    $progress_fd.flush
end

begin
base_vm_args = [
    $qemu_binary,
    '--device', 'virtio-blk,drive=sys',
    '--m', '1G',
    '--device', JSON.unparse({
        driver: 'vhost-user-fs-pci',
        'queue-size': 1024,
        chardev: 'vfsdsock',
        tag: $vfs_tag,
    }),
    '--object', JSON.unparse({
        'qom-type': 'memory-backend-memfd',
        id: 'mem',
        size: 1 * 1024 * 1024 * 1024,
    }),
    '--machine', "#{$qemu_machine_type},accel=kvm,memory-backend=mem",
    '--display', 'none',
]

if $vm_boot_snapshot && File.file?($vm_boot_snapshot)
    snapshot_args = ['--incoming', "exec:cat #{$vm_boot_snapshot.shellescape}"]
else
    snapshot_args = []
end

vm_s = VM.new(*base_vm_args,
              '--blockdev', JSON.unparse({
                  driver: 'qcow2',
                  'node-name': 'sys',
                  file: {
                      driver: 'file',
                      filename: "#{$shared_dir}/overlay.qcow2",
                  },
              }),
              '--chardev', "socket,id=vfsdsock,path=#{$instances[0][:sock]}",
              *snapshot_args,
              '--name', 'Source',
              normal_vm: true, keep_stderr: true, ssh: 2345 + $job_index * 8, kvm: true)

vm_s.wait_ssh($guest_user, $guest_password)

vm_s.qmp.migrate_set_capabilities(capabilities: [
    { capability: 'events', state: true },
])

if $vm_boot_snapshot && !File.file?($vm_boot_snapshot)
    vm_s.qmp.migrate(uri: "exec:cat>#{$vm_boot_snapshot.shellescape}")

    while true
        case vm_s.qmp.event_wait('MIGRATION')['data']['status']
        when 'completed'
            break
        when 'failed'
            $stderr.puts('Failed to save snapshot')
            exit 1
        end
    end

    if $vm_boot_snapshot_complete
        IO.write($vm_boot_snapshot_complete, '')
    end

    vm_s.qmp.cont
end

vm_s.ssh("mkdir #{$guest_mountpoint.shellescape} || sudo mkdir #{$guest_mountpoint.shellescape}")
vm_s.ssh("mount -t virtiofs #{$vfs_tag.shellescape} #{$guest_mountpoint.shellescape} 2>/dev/null || sudo mount -t virtiofs #{$vfs_tag.shellescape} #{$guest_mountpoint.shellescape}")

vm_s.qmp.migrate_set_capabilities(capabilities: [
    { capability: 'pause-before-switchover', state: true },
])

$instances[0][:vm] = vm_s

def run_ops(instance, phase, ops)
    ops.each do |op|
        case op['on'] or 'vm'
        when 'vm'
            puts("--- [#{instance[:name]}, #{phase}] #{op['name'] ? op['name'] : op['cmd']} ---")
            instance[:vm].ssh(op['cmd'], background: op['background'])
        when 'host'
            puts("--- [host, #{phase}] #{op['name'] ? op['name'] : op['cmd']} ---")
            if op['background']
                if !fork
                    Dir.chdir($shared_dir)
                    exec(op['cmd'])
                end
            else
                cwd = Dir.pwd
                Dir.chdir($shared_dir)
                system(op['cmd'])
                Dir.chdir(cwd)
            end
        else
            $stderr.puts("Unrecognized op context #{op['on']}")
        end
    end
end

success = true

$migrations.each.with_index do |migration, i|
    src = $instances[i]
    dst = $instances[i + 1]

    if $progress_fd
        $progress_fd.puts(i % 2 == 0 ? '🌒' : '🌖')
        $progress_fd.flush
    end

    dst[:vm] =
        VM.new(*base_vm_args,
               '--blockdev', JSON.unparse({
                   driver: 'qcow2',
                   'node-name': 'sys',
                   file: {
                       driver: 'file',
                       filename: "#{$shared_dir}/overlay.qcow2",
                   },
               }),
               '--chardev', "socket,id=vfsdsock,path=#{dst[:sock]}",
               '--incoming', $migration_url,
               '--name', 'Destination',
               normal_vm: true, keep_stderr: true, ssh: 2345 + i + 1 + $job_index * 8, kvm: true)

    dst[:vm].qmp.migrate_set_capabilities(capabilities: [
        { capability: 'events', state: true },
        { capability: 'pause-before-switchover', state: true },
    ])

    run_ops(src, 'before migration', migration[:pre])

    if $progress_fd
        $progress_fd.puts(i % 2 == 0 ? '🌓' : '🌗')
        $progress_fd.flush
    end

    src[:vm].qmp.migrate(uri: $migration_url)
    run_ops(src, 'during migration', migration[:during])

    while true
        status = src[:vm].qmp.event_wait('MIGRATION')['data']['status']
        if status == 'pre-switchover'
            if $progress_fd
                $progress_fd.puts(i % 2 == 0 ? '🌔' : '🌘')
                $progress_fd.flush
            end
            run_ops(src, 'pre-switchover', migration[:pre_switchover])
            src[:vm].qmp.migrate_continue(state: 'pre-switchover')
        end
        break if ['completed', 'failed'].include?(status)
    end

    if status == 'failed'
        while src[:vm].qmp.query_status['status'] == 'finish-migrate'
            sleep 0.1
        end
    end

    begin
        raise "Migration failed on source side (#{src[:name]})" if status == 'failed'

        while true
            status = dst[:vm].qmp.event_wait('MIGRATION')['data']['status']
            break if ['completed', 'failed'].include?(status)
        end

        raise "Migration failed on destination side (#{dst[:name]})" if status == 'failed'

        if $progress_fd
            if i == $migrations.length - 1
                $progress_fd.puts(i % 2 == 0 ? '🌝' : '🌚')
            else
                $progress_fd.puts(i % 2 == 0 ? '🌕' : '🌑')
            end
            $progress_fd.flush
        end

        dst[:vm].wait_ssh($guest_user, $guest_password)
        puts '(Migration successful)'
        src[:vm].qmp.quit
        src[:vm].wait

        run_ops(dst, 'after migration', migration[:post])
    rescue Exception => e
        if $progress_fd
            if i == $migrations.length - 1
                $progress_fd.puts(i % 2 == 0 ? '🌝' : '🌚')
            else
                $progress_fd.puts(i % 2 == 0 ? '🌕' : '🌑')
            end
            $progress_fd.flush
        end

        begin
            dst[:vm].qmp.quit
        rescue Exception
            # Ignore failing to send quit
        end
        dst[:vm].wait
        Process.waitpid(dst[:viofsd])

        puts "(Migration failed (#{e}), is source side (#{src[:name]}) still running?)"
        src[:vm].qmp.cont
        run_ops(src, 'after failed migration', migration[:post_failed])
        src[:vm].qmp.quit
        src[:vm].wait
        success = false
        break
    end
end

if success
    $instances[-1][:vm].qmp.quit
    $instances[-1][:vm].wait
end

rescue Exception => e
    $instances.each.with_index do |inst, i|
        inst[:vm].kill if inst[:vm]
        Process.kill('TERM', inst[:viofsd])
    end

    system("rm -f /tmp/qemu.rb-*")

    raise e.inspect
end
