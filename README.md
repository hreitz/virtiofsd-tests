Running migration tests
=======================

The *migration.rb* script runs a multitude of test cases for virtio-fs migration.  To use it, you need a Linux VM guest
image that allows SSH login with username and password, and either password-less `sudo` or the ability to mount the
virtio-fs filesystem as the default user:
```
$ ./migration.rb \
    --guest-image ~/Downloads/Arch-Linux-x86_64-basic-20240315.221711.qcow2 \
    --guest-login arch:arch \
    --virtiofsd-command ../virtiofsd/target/debug/virtiofsd \
    --scratch-dir /tmp/virtiofsd-migration-test \
    --jobs 4
```

The given guest image is not modified, every VM instance gets its own ephemeral overlay file.

Some test cases may require running virtiofsd with root privileges (to be able to use file handles).  This is done via
`sudo -n`, and if that does not work, the respective test cases will be cancelled.  To allow this to work, usually it is
sufficient to run `sudo true` in the same terminal before running the migration tests (see “credential caching” in the
`sudo` man page).
